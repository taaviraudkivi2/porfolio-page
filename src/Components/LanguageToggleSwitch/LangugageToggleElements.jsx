import styled from "styled-components";

let toggleBackground = "#000";
let textColor = "#fff";

export const ToggleContainer = styled.div`
    z-index: 0;
    position: absolute;
    top: 10px;
    right: 20px;

    @media (min-width: 768px) {
      
    }

    @media (min-width: 1025px) {
        right: 40px;
    }
`
export const InputWrapper = styled.label`
    position: relative;
    display: flex;
    justify-content: center;
    text-align: center;
`

export const Input = styled.input`
    position: absolute;
    left: -9999px;
    top: -9999px;

    &:checked + span {
        &::before {
            left: 50px;
        }
    }

    @media (min-width: 1025px) {
        &:checked + span {
        &::before {
            left: 55px;
        }
    }
    }
`

export const Slider = styled.span`
    display: flex;
    justify-content: center;
    text-align: center;
    cursor: pointer;
    width: 105px;
    border-radius: 100px;
    position: relative;
    transition: background-color 0.2s;
    color: ${textColor};
    font-family: Candara;
    font-weight: 700;
    font-size: 16px;

    &::before {
        content: "";
        position: absolute;
        top: 13px;
        left: 1px;
        width: 53px;
        height: 21px;
        border-radius: 45px;
        transition: 0.2s;
        background: ${toggleBackground};
        box-shadow: 0 2px 4px 0 rgba(0, 35, 11, 0.2);
    }
    
    @media (min-width: 768px) {
        font-size: 18px;

        &::before {
            top: 17px;
            left: -5px;
            width: 57px;
            height: 24px;
        }
    }

    @media (min-width: 1025px) {
        font-size: 20px;

        &::before {
            top: 20.5px;
            left: -7px;
            width: 57px;
            height: 24px;
        }
    }
`;

export const LangText = styled.p`
    padding: 0 7px;
    z-index: 1;
    letter-spacing: 3px;

    @media (min-width: 768px) {
        padding: 0 8px;
    }

    @media (min-width: 1025px) {
        padding: 0 10px;
    }
`;