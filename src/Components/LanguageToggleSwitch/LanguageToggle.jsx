import React, {useState} from 'react'
import BottomNavigation from '../BottomNavigation/BottomNavigation';
import { Input, InputWrapper, Slider, ToggleContainer, LangText} from './LangugageToggleElements';

function LanguageToggle() {
    const [checked, setChecked] = useState(false)

    return(
        <div>
            {checked
            ? <BottomNavigation lang={"est"}/>:
            <BottomNavigation lang={"eng"}/>}
            <ToggleContainer>
                <InputWrapper>
                    <Input type="checkbox" checked={checked} onChange={e => setChecked(e.target.checked)}/>
                    <Slider>
                        <LangText style={{color: checked ? "#000" : "#fff"}}>ENG</LangText>
                        <LangText style={{color: checked ? "#fff" : "#000"}}>EST</LangText>
                    </Slider>
                </InputWrapper>
            </ToggleContainer>
        </div>
)} ;


export default LanguageToggle
